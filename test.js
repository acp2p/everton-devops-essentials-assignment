function sum(numberOne, numberTwo) {
    const result = numberOne + numberTwo;
    if(result !== 15) {
        throw new Error(`Test failed \n Expected: 15 \n Received: ${ result }`);
    }
}

sum(10, 5);